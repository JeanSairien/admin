# tools/dependencies required for development or running tests
autopep8
autoflake
isort
pylama
pytest-cov
pytest-django
# counterpart of requests for mock responses
pytest-responses
# output logging when test fails
pytest-logging
django-coverage
django-extensions
django-debug-toolbar
django-debug-toolbar-request-history

# # dashboard for celery introspection
# # run failmap-admin celery flower
# # go to http://localhost:5555/
# celery-flower
