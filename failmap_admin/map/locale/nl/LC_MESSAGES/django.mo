��    1      �  C   ,      8     9     G     O     W  	   [     e     q     z     �     �     �     �  
   �     �     �     �     �                     $     +     2     ;     N     [     x  
   ~     �     �     �  !   �  
   �     �       "        <     V     o     �     �     �     �     �     �     �     �     �  @  �     %     4     :     @     G     T  	   [  �   e     T	     Y	  M   k	     �	     �	     �	     �	     �	     �	  �   �	     �
     �
     �
  	   �
     �
  >   �
               9  )   ?      i  `   �     �  )        /     ;  
   L  !   W     y     �     �     �     �     �     �     �     �     �                              '       ,                )   *         /      &   $                               !      +                 #                  (      -      	   1              "      0      
   .   %                                        About Failmap Address Average Bad Data from Explanation Fail Map Fail Map Introduction Good Internet addresses Internet addresses Explained Intro Last check Number Numbers Organization Organizations Organizations Explained Points Rank Report Result Services Services explained Services faq Show report for organization Since Site Title Technical TLS Technical stats TLS explained Technical stats headers Technical stats headers explained Technology Terrible Addresses The Numbers These are the worst addresses on:  This is the top fail from This is the top win from Toggle navigation Top Fail Top Win! Total Url Voor burgers Voor ontwikkelaars Voor organisaties When congratulations Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-11-05 12:39+0000
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Over Faalkaart Adres Matig Slecht Gegevens van Uitleg Faalkaart Faalkaart geeft inzicht in hoe veilig uw gemeente is richting het internet. Er wordt gekeken hoe veilig de gemeente haar verbindingen heeft ingericht. Het is belangrijk dat dit goed gebeurt omdat hierover ook uw gegevens worden verstuurd. Goed Internet adressen Van de onderstaande adressen hebben we een beveilingsstatus weten te bepalen. Introductie Laatste controle Aantal Cijfers Organisatie Organisaties Organisaties hebben vaak meerdere internet adressen. Alle fouten op deze adressen bij elkaar opgeteld bepalen of de organisatie haar beveiliging op orde heeft. Punten Notering Verslag Resultaat Diensten Een adres kan allerlei diensten aanbieden, waaronder websites. Meer over Diensten Toon verslag voor organisatie Sinds Faalkaart - Geeft inzicht in beveiliging. Integriteit en vertrouwelijkheid Goede versleuteling garandeert dat informatie niet door anderen gelezen of aangepast kan worden. Veiligheid inhoud website Of de website diverse aanvallen voorkomt. Beveiliging Slechte adressen De Cijfers Dit zijn de slechtste adressen op Dit is de top faal van Dit is de top win van In of uitklappen Top faal Top Win! Aantal Internet Adres Voor burgers Voor ontwikkelaars Voor organisaties Tijdstip gefeliciteerd 